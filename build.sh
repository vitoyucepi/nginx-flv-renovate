#!/usr/bin/env bash

set -eux

main() {
  local version=$1
  cd "containers/${version}"
  grep -i '^from' Dockerfile | grep -vP '(scratch)' | cut -d ' ' -f2 | sort -u | xargs -I {} docker pull {}
  docker build -t "registry.gitlab.com/vito-containers/nginx-flv:${version}" .
}

main "$@"
